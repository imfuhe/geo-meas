# calDistanceFrom2Points

## 功能

该函数用于计算空间任意两点的距离。

## 原理

空间两点距离如图所示：

![a74e22b63e31e12fa8969839624ea87.png](https://s2.loli.net/2022/01/23/LhHOaRky2gpWw7Q.png)


设两点坐标$P_1(x_1,y_1,z_1)$和$P_2(x_2,y_2,z_2)$，根据空间两点距离公式：
$$
Distance = \sqrt{(x_2-x_1)^2+(y_2-y_1)^2+(z_2-z_1)^2}
$$
利用上述公式可计算出空间两点距离。

## 使用

### 输入

空间坐标系下任意两点坐标$P_1(x_1,y_1,z_1)$和$P_2(x_2,y_2,z_2)$。

### 输出

空间两点距离$Distance$。

### DEMO

```python
import geomeas as gm
import numpy as np
import math

P_1 = np.array([13.66, 121.55, 0])
P_2 = np.array([101.84, 121.55, 136.68])

print(gm.Distance().calDistanceFrom2Points(P_1, P_2))
```

# 链接

[代码链接](https://gitee.com/huangzhexiaohao/geo-meas/blob/master/src/geomeas.py)

