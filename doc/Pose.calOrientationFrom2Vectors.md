# calOrientationFrom2Vectors

## 功能

根据两个不共线的矢量分别在两个坐标系下的矢量坐标求解这两个坐标系之间的旋转矩阵。

## 原理

若已知两个不共线的矢量分别在两个坐标系下的方向矢量，即可以方向矢量构建参考坐标系从而求得两个坐标系之间的旋转矩阵。

如图所示，两个坐标系分别为坐标系C、坐标系S，两个矢量分别为V1、V2，参考坐标系（坐标系n）根据矢量V1、V2建立。

![双矢量原理图](双矢量原理图.png)

参考坐标系在坐标系C下的正交坐标基为
$$
\left\{ \begin{aligned} a & = \ V_1^c \\ b & = \ \frac{(V_1^c \times V_2^c)}{|V_1^c \times V_2^c|} \\ c & = \ a \times b \end{aligned} \right.
$$
坐标系C到参考坐标系的姿态转换矩阵为
$$
R_c^n = \begin{bmatrix} a^T\\ b^T\\c^T\end{bmatrix}
$$
同理，参考坐标系在坐标系S下的正交坐标基为
$$
\left\{ \begin{aligned} A & = \ V_1^s \\ B & = \ \frac{(V_1^s \times V_2^s)}{|V_1^s \times V_2^s|} \\ C & = \ A \times B \end{aligned} \right.
$$
坐标系S到参考坐标系的姿态转换矩阵为
$$
R_s^n = \begin{bmatrix} A^T\\ B^T\\C^T\end{bmatrix}
$$
坐标系S到坐标系C的旋转矩阵
$$
R_s^c=(R_c^n)^{-1} \cdot R_s^n
$$

## 使用

### 输入

两个矢量在坐标系s下的坐标：$$Vs1(xs1,ys1,zs1)$$，$$Vs2(xs2,ys2,zs2)$$；

在坐标系n下的坐标：$$Vn1(xn1,yn1,zn1)$$，$$Vn2(xn2,yn2,zn2)$$。

### 输出

坐标系n到坐标系s的旋转矩阵$$R_n^s$$。

### DEMO

```python
import geomeas as gm
import numpy as np

Vs1 = np.array([0.55397988, 0.82791962, -0.08749517])
Vs2 = np.array([0.02063334, -0.26258813, -0.96468738])
Vn1 = np.array([0.97066373, 0.20744552, 0.12156592])
Vn2 = np.array([0, 0, -1])

print(gm.Pose().calOrientationFrom2Vectors(Vs1, Vs2, Vn1, Vn2))

```

# 链接

https://gitee.com/huangzhexiaohao/geo-meas/blob/master/src/geomeas.py