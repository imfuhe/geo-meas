# calCoordinateFrom2Lines

## 功能

已知两直线的“点向式”方程，分别输入两条直线的点和方向向量，计算两直线的交点坐标。

## 原理

如图所示，设空间上两条直线相交，对应的点坐标$P(x_1,y_1,z_1)$、$Q(x_2,y_2,z_2)$，对应的方向向量$\vec{s_1}(m_1,n_1,p_1)$、$\vec{s_2}(m_2,n_2,p_2)$，两条直线相交于$M(x,y,z)$。

![直线求交点.png](https://s2.loli.net/2022/01/22/KRl6riYdCX8Bbav.png)

根据空间直线的一点与该直线的方向向量可得到直线的点向式方程：
$$
l_1:\frac{x-x_1}{m_1}=\frac{y-y_1}{n_1}=\frac{z-z_1}{p_1}\\
l_2:\frac{x-x_2}{m_2}=\frac{y-y_2}{n_2}=\frac{z-z_2}{p_2}
$$
利用中间参数$t$可将直线上某点坐标表示为：
$$
x=x_1+m_1t\\
y=y_1+n_1t\\
z=z_1+p_1t
$$
将该点坐标带入$l_2$中，计算出$t$的值为：
$$
t=\frac{m_2(y_1-y_2)-n_2(x_1-x_2)}{m_1n_2-m_2n_1}
$$
将$t$代入该点坐标即可求出该点坐标值。

## 使用

### 输入

直线$l_1$的任意点坐标$P(x_1,y_1,z_1)$和该直线的方向向量$\vec{s_1}(m_1,n_1,p_1)$；

直线$l_2$的任意点坐标$Q(x_2,y_2,z_2)$和该直线的方向向量$\vec{s_2}(m_2,n_2,p_2)$。

### 输出

空间两直线的交点$M(x,y,z)$。

### DEMO

```python
import geomeas as gm
import numpy as np

P_1 = np.array([-108.45, 174.45, 0])
s_1 = np.array([-335.6, -77.27, 0])
Q_1 = np.array([227.15, 174.45, 0])
s_2 = np.array([335.6, -77.27, 0])

print(gm.Coordinate().calCoordinateFrom2Lines(P_1, s_1, Q_1, s_2))
```

# 链接

[代码链接](https://gitee.com/huangzhexiaohao/geo-meas/blob/master/src/geomeas.py)