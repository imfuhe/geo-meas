# calVectorFrom2Planes

## 功能

利用两平面的法向量做”叉乘“运算获得两平面交线的方向向量。

## 原理

如图所示，两平面相交关系为：

![3418f486ea4e385cf998b6fb4a8aed3.png](https://s2.loli.net/2022/01/22/pHj4RrfEI6wh7oc.png)

图中$\vec{n_1}$为平面1的法向量，$\vec{n_2}$为平面2的法向量，$\vec{l}$为两平面交线的方向向量。根据丘维声所著《解析几何（第三版）》第30-36页可知，该方向向量可由两平面法向量进行叉乘运算得到，公式为：
$$
\vec{l}=\vec{n_1}\times\vec{n_2}
$$
由平面一般式方程可知，平面的法向量为：$\vec{n}=(a,b,c)$，利用两相交平面的方程参数即可进行法向量”叉乘“，获得交线的方向向量$\vec{l}=(m,n,p)$，利用该直线上任意一点坐标$(x_1,y_1,z_1)$可得到该支线的”点法式“方程：
$$
\frac{x-x_1}{m}=\frac{y-y_1}{n}=\frac{z-z_1}{p}
$$


## 使用

### 输入

两相交平面方程一般式的参数$Param_1(a_1,b_1,c_1,d_1)$、$Param_2(a_2,b_2,c_2,d_2)$。

### 输出

两平面交线方向向量$\vec{l}(m,n,p)$。

### DEMO

```python
import geomeas as gm
import numpy as np

Param_1 = np.array([0, 0, -4361.9337, 362040.4971])
Param_2 = np.array([-2180.41, 6939.63, 2165.499, -283785.5822])

print(gm.Vector().calVectorFrom2Planes(Param_1, Param_2))
```

# 链接

[代码链接](https://gitee.com/huangzhexiaohao/geo-meas/blob/master/src/geomeas.py)