# calVectorFrom2Points

## 功能

利用空间两点坐标计算该直线的方向向量$\vec{s}(m,n,p)$。

## 原理

如图所示，空间任意两点$Point_{1}(x_1,y_1,z_1)$和$Point_{2}(x_2,y_2,z_2)$。

![422b4a043c61013b3f03b5c4b20539a.png](https://s2.loli.net/2022/01/23/GPg6mfhZ5liXYBK.png)

根据上图可知空间直线的方向向量$s$可由空间两点做差值获得：
$$
\vec{s}=Point_{1}-Point_{2}=(x_1-x_2,y_1-y_2,z_1-z_2)
$$

## 使用

### 输入

空间任意两点坐标$Point_{1}(x_1,y_1,z_1)$、$Point_{2}(x_2,y_2,z_2)$。

### 输出

上述两点所在直线的方向向量$\vec{s}(m,n,p)$。

### DEMO

```python
import geomeas as gm
import numpy as np

Point_1 = np.array([227.15, 174.45, 0])
Point_2 = np.array([-108.45, 251.72, 0])

print(gm.Vector().calVectorFrom2Points(Point_1, Point_2))
```

# 链接

[代码链接](https://gitee.com/huangzhexiaohao/geo-meas/blob/master/src/geomeas.py)