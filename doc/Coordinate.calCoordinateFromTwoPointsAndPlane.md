# calCoordinateFromTwoPointsAndPlane

## 功能

该函数用于计算空间直线与空间平面的交点坐标。

## 原理

空间直线与平面交点示意如下图所示：

![d6105f59022bec0c1922e2cdc1211f8.png](https://s2.loli.net/2022/01/23/VEcoWHfmjhrlP3a.png)


利用空间直线上任意两点求得其方向向量：
$$
LineVector=(x_1-x_2,y_1-y_2,z_1-z_2)=(m,n,p)
$$
根据丘维声所著《解析几何（第三版）》第60-61页可将空间直线方程表示为“点向式”方程：
$$
\frac{x-x_1}{m}=\frac{y-y_1}{n}=\frac{z-z_1}{p}
$$
利用中间参数可将上式表示为：
$$
\left\{
\begin{array}{c}
x=mt+x_1\\
y=nt+y_1\\
z=pt+z_1
\end{array}
\right.
$$
通过上式与平面方程联立可计算出中间参数为：
$$
t=\frac{-ax_1-by_1-cz_1-d}{am+bn+cp}
$$
利用计算出的中间参数反求直线与平面交点$E(x,y,z)$。至此，空间直线与平面交点坐标求解完毕。

## 使用

### 输入

根据上述函数计算原理可知，该函数的输入为空间直线上任意两点坐标与平面方程参数：
$$
Input():
\left\{
\begin{array}{c}
P_1(x_1,y_1,z_1)\\
P_2(x_2,y_2,z_2)\\
(a,b,c,d) \in ax+by+cz+d=0
\end{array}
\right.
$$

### 输出

根据上述函数计算流程可知，该函数的输出为空间直线与平面的交点：
$$
Output:E(x,y,z)
$$

### DEMO

```python
import geomeas as gm
import numpy as np

P1 = np.array([-670.13, 1477.30, -1576.88])
P2 = np.array([-761.34, 914.65, -1576.88])
PlaneParams = np.array([0.00000000e+00, 5.54216347e+05, 0.00000000e+00, -3.57990507e+08])

print(gm.Coordinate().calCoordinateFromTwoPointsAndPlane(P1, P2, PlaneParams))
```

# 链接

[代码链接](https://gitee.com/huangzhexiaohao/geo-meas/blob/master/src/geomeas.py)

