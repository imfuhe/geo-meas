# calPlaneEquationFrom3Points

## 功能

该函数为利用某平面任意三点计算该平面的法向量，得到平面方程系数。

## 计算原理

利用三点求取平面方程系数原理如下图所示：

![73a3944b4f89ccb7afc150495a4faff.png](https://s2.loli.net/2022/01/23/LrsVMU4gAtOGbnW.png)



利用平面上任意三点，得到平面上任意两向量，通过求取两向量的向量积可得到该平面的法向量,如下式所示：
$$
\vec{n}=\vec{P_1P_2}\times\vec{P_1P_3}
$$
根据丘维声所著《解析几何（第三版）》第48-51页可知：该平面法向量所对应的元素$a$，$b$，$c$分别为平面方程一般式所对应的$a$，$b$，$c$。平面方程一般式如下式所示：
$$
ax+by+cz+d=0
$$
将平面方程改写成“点法式”，并与一般式方程联立：
$$
\left\{
\begin{array}{c}
a(x-x_1)+b(y-y_1)+c(z-z_1)=0\\
ax+by+cz+d=0
\end{array}
\right.
$$
求出平面一般式方程中的$d$为：
$$
d=-ax_1-by_1-cz_1
$$
至此，利用平面上三点求平面方程参数的计算完毕。

## 使用

### 输入

根据函数计算原理可知，该函数的输入为平面上任意三点的坐标：
$$
Input():
\left\{
\begin{array}{c}
Point1(x_1,y_1,z_1)\\
Point2(x_2,y_2,z_2)\\
Point3(x_3,y_3,z_3)
\end{array}
\right.
$$

### 输出

根据计算流程可知，该函数的输出为平面方程的参数：
$$
Output()：(a,b,c,d)\in ax+by+cz+d=0
$$

### DEMO

```python
import geomeas as gm
import numpy as np

Point1 = np.array([-2919.13, 654.94, 1100])
Point2 = np.array([-2919.13, 0, 1100])
Point3 = np.array([-2438.31, 0, 0])

print(gm.Plane().calPlaneEquationFrom3Points(Point1, Point2, Point3))
```

# 链接

[代码链接](https://gitee.com/huangzhexiaohao/geo-meas/blob/master/src/geomeas.py)

