from asyncio.windows_events import NULL
import random
import cv2
import numpy as np
from numpy.core.fromnumeric import shape
from PIL import Image
import time
import timeit

COLORS = {'red':(0,0,255),
         'blue':(255,0,0),
         'white':(255,255,255)}

#plotly
#import plotly as py
#import plotly.graph_objs as go

#deep learning
#import keras
#from keras.datasets import mnist

# keras入门测试
''''''
class TestKeras():
    def init(self):
        #brief:复现手写字符识别
        
        print('minist test！！！')

#数字图像处理课程课堂演示代码
class Basicproc(): 
    
    img1 = None
    img2 = None
    def calSimThroreticalCenterOneTime(self, w:int,h:int, radius:int, randomrange_up:int):
        #def calSimThroreticalCenter(self, w, h, radius, randomrange_up):
        '''
        作者：HZ；
        日期：2022-4-16；
        功能：计算模拟理论质心，程序执行一次
        参数：
            w：模拟图像宽度；
            h：模拟图像高度；
            radius：模拟光斑半径；
            randomrange_up：随机范围；
        '''
        W = w#宽度（列数）
        H = h#高度（行数）
        CENTER = (int(W/2), int(H/2))#理论中心
        R = radius
        RANDOMRANGE_UP = randomrange_up#不能超过1024
        centerlist = []
        img = self.genEmptyImage(w=W, h=H, opt='color')
        title = 'empty img' + str(img.shape)
        
        rnd = (np.random.randint(1,RANDOMRANGE_UP)-int(RANDOMRANGE_UP/2), np.random.randint(1,RANDOMRANGE_UP)-int(RANDOMRANGE_UP/2))#随机生成不同位置
        cxcy = (CENTER[0]+rnd[0],CENTER[1]+rnd[1])
        cv2.circle(img, center=cxcy, radius=R, color=(255,255,255), thickness=-1)
        imgray = cv2.cvtColor(img, code=cv2.COLOR_RGB2GRAY)

        #轮廓法提取光斑质心
        ret, thresh = cv2.threshold(imgray, 127, 255, cv2.THRESH_BINARY)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #找全部质心
        for index in range(len(contours)):
            M = cv2.moments(contours[index])
            cx = round(M['m10'] / M['m00'], 3)  # 算质心
            cy = round(M['m01'] / M['m00'], 3)    
            centerlist.append([cx, cy])
        #封装函数结束
        cv2.drawContours(img,contours=contours,contourIdx=-1,color=COLORS['red'],thickness=2)    
        cv2.drawMarker(img,position=cxcy,color=COLORS['red'],markerType=cv2.MARKER_CROSS,thickness=2)
        #print('理论中心：',cxcy,' 计算质心：', centerlist)
        '''
        cv2.imshow(title, img)
        k = cv2.waitKey(0)
        if k == 27:
            cv2.destroyAllWindows()
        '''
        return [img, cx, cy]
    
    def calSimThroreticalCenter(self, w:int,h:int, radius:int, randomrange_up:int):
    #def calSimThroreticalCenter(self, w, h, radius, randomrange_up):
        '''
        作者：HZ；
        日期：2022-4-15；
        功能：计算模拟理论质心，程序持续执行，按空格继续
        参数：
            w：模拟图像宽度；
            h：模拟图像高度；
            radius：模拟光斑半径；
            randomrange_up：随机范围；
        '''
        W = w#宽度（列数）
        H = h#高度（行数）
        CENTER = (int(W/2), int(H/2))#理论中心
        R = radius
        RANDOMRANGE_UP = randomrange_up#不能超过1024
        i = 0
        #for i in range(1000):
        while True:
            centerlist = []
            img = self.genEmptyImage(w=W, h=H, opt='color')
            title = 'empty img' + str(img.shape)
            
            rnd = (np.random.randint(1,RANDOMRANGE_UP)-int(RANDOMRANGE_UP/2), np.random.randint(1,RANDOMRANGE_UP)-int(RANDOMRANGE_UP/2))#随机生成不同位置
            cxcy = (CENTER[0]+rnd[0],CENTER[1]+rnd[1])
            cv2.circle(img, center=cxcy, radius=R, color=(255,255,255), thickness=-1)
            imgray = cv2.cvtColor(img, code=cv2.COLOR_RGB2GRAY)

            #轮廓法提取光斑质心
            ret, thresh = cv2.threshold(imgray, 127, 255, cv2.THRESH_BINARY)
            contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            #找全部质心
            for index in range(len(contours)):
                M = cv2.moments(contours[index])
                cx = round(M['m10'] / M['m00'], 3)  # 算质心
                cy = round(M['m01'] / M['m00'], 3)    
                centerlist.append([cx, cy])
            #封装函数结束
            
            cv2.drawContours(img,contours=contours,contourIdx=-1,color=COLORS['red'],thickness=2)    
            cv2.drawMarker(img,position=cxcy,color=COLORS['red'],markerType=cv2.MARKER_CROSS,thickness=2)
            print('理论中心：',cxcy,' 计算质心：', i,centerlist)
            i = i+1
            #cv.imwrite('empty.png', img)
            cv2.imshow(title, img)
            k = cv2.waitKey(0)
            if k == 27:
                cv2.destroyAllWindows()
                break
    def sp_noise(self, image,prob):
        '''
        添加椒盐噪声
        prob:噪声比例
        https://www.jb51.net/article/162073.htm
        '''
        output = np.zeros(image.shape,np.uint8)
        thres = 1 - prob 
        for i in range(image.shape[0]):
            for j in range(image.shape[1]):
                rdn = random.random()
                if rdn < prob:
                    output[i][j] = 0
                elif rdn > thres:
                    output[i][j] = 255
                else:
                    output[i][j] = image[i][j]
        return output
 
    def gasuss_noise(self, image, mean=0, var=0.001):
        ''' 
            添加高斯噪声
            mean : 均值 
            var : 方差
            https://www.jb51.net/article/162073.htm
        '''
        image = np.array(image/255, dtype=float)
        noise = np.random.normal(mean, var ** 0.5, image.shape)
        out = image + noise
        if out.min() < 0:
            low_clip = -1.
        else:
            low_clip = 0.
        out = np.clip(out, low_clip, 1.0)
        out = np.uint8(out*255)
        #cv.imshow("gasuss", out)
        return out
    
    def genEmptyImage(self, w:(int), h:(int), opt:(str)):
        '''f
        作者：HZ；
        日期：2022.4.5；
        功能：生成0灰度值的标准空图像；
        输入：w,图像宽；h，图像高；grayvalue：底色灰度值；
        输出：img，生成的空白图像；
        DEMO:
        '''
        if opt == 'gray':
            img=np.zeros((h,w,1), np.uint8)#h行，w列
        elif opt == 'color':
            img=np.zeros((h,w,3), np.uint8)
        else:
            img = None
        return img
    
    
    def readimg(self):
        i = 0
        flag_list = [cv2.IMREAD_ANYCOLOR, cv2.IMREAD_UNCHANGED, cv2.IMREAD_LOAD_GDAL, cv2.IMREAD_ANYDEPTH, cv2.IMREAD_COLOR, cv2.IMREAD_REDUCED_COLOR_8]
        #help(cv2.destroyAllWindows)
        img = cv2.imread('特斯拉.jpg',flag_list[-1])
        #img = cv2.imread('tesla.jpg', 65)

        for flag in flag_list:
            print(i, flag)
            i = i+1

        #cv2.namedWindow('image', cv2.WINDOW_NORMAL)
        cv2.imshow('image',img)
        
        '''
        ret = cv2.waitKey(1000000000)
        print('ret:', ret)
        cv2.destroyAllWindows()
        '''
        
        k = cv2.waitKey(0)
        #print('k:', k)
        
        if k == 27:
            cv2.destroyAllWindows()
        elif k == ord('s'):
            cv2.imwrite('lalal.png', img)
            cv2.destroyAllWindows()
        
    def drawline(self):
        img=np.zeros((512,512,3), np.uint8)
        cv2.line(img,(0,0),(511,511),(255,233,18),1,lineType=cv2.LINE_AA)
        cv2.rectangle(img,(200,0),(400,128),(55,233,18),3)
        cv2.circle(img,(447,63), 63, (0,0,255), 3)
        cv2.ellipse(img,(256,256),(100,50),0,0,180,(255,255,0),4)
        pts=np.array([[[10,10],[70,10],[100,100],[10,80]]], np.int32)
        print(pts, pts.shape)
        cv2.polylines(img, pts, isClosed=1, color=(255,255,0), thickness= 2)
        cv2.fillPoly(img,pts,color=(0,255,0),lineType=cv2.LINE_4)
        font=cv2.FONT_HERSHEY_PLAIN
        cv2.putText(img,'opencv',(10,500), font, 4,(255,255,255),2)
        
        cv2.imshow('image',img)
        cv2.waitKey(0)
    
    def basicoperation(self):
        #2021-9-15图像的基础操作
        
        img = cv2.imread('特斯拉.jpg',cv2.IMREAD_COLOR)
        '''
        print('type:', type(img), 'shape:', img.shape,'size:', img.size, np.size(img), 'dtype:', img.dtype)
        before_t = time.time()
        img[20,20,0]=100
        #img=np.zeros((2,2,3), np.uint8)
        #px = img.item(20,20,2)
        #img.itemset((20,20,0),100)
        after_t = time.time()
        print('period1:', (after_t - before_t)*1000)
        #print('像素值：', img[20,20])
        #print('像素值：', img[20,20,2])
        # roi
        car = img[350:540, 510:650]
        img[1:191,1:141] = car
        
        cv2.imshow('img:', img)
        k = cv2.waitKey(0)
        cv2.destroyAllWindows()
        '''

        '''
        # 拆分
        before_t = time.time()
        #b, g, r = img[:,:,0], img[:,:,1], img[:,:,2]
        b, g, r = cv2.split(img)
        after_t = time.time()
        print('period2:', 1000*(after_t - before_t), after_t, before_t)
        
        
        cv2.imshow('img:', g)
        k = cv2.waitKey(0)
        cv2.destroyAllWindows()
        '''
        
        #图像扩边
        BLUE = [0, 0, 255]
        constant= cv2.copyMakeBorder(img,200,10,10,10,cv2.BORDER_CONSTANT,value=BLUE)
        
        cv2.imshow('img:', constant)
        k = cv2.waitKey(0)
        cv2.destroyAllWindows()
        
    def mathoperation(self):
        '''
        #图像的算数运算
        x = np.uint8([240])
        y = np.uint8([30])
        print(cv2.add(x,y), x+y)
        
        #图像混合
        img2=cv2.imread('1.jpg')
        img1=cv2.imread('2.jpg')
        
        dst=cv2.addWeighted(img1,0.2,img2,0.7,150)
        cv2.imshow('dst',dst)
        cv2.waitKey(0)
        cv2.destroyAllWindow()

        '''
        #按位运算
        img1 = cv2.imread('特斯拉.jpg',cv2.IMREAD_COLOR)
        img2 = cv2.imread('opencvlogo.jpg',cv2.IMREAD_COLOR)
        
        # I want to put logo on top-left corner, So I create a ROI
        rows,cols,channels = img2.shape
        roi = img1[0:rows, 0:cols ]
        # Now create a mask of logo and create its inverse mask also
        img2gray = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
        ret, mask = cv2.threshold(img2gray, 180, 255, cv2.THRESH_BINARY)
        mask_inv = cv2.bitwise_not(mask)
        # Now black-out the area of logo in ROI
        # 取 roi 中与 mask 中不为零的值对应的像素的值，其他值为 0 # 注意这里必须有 mask=mask 或者 mask=mask_inv, 其中的 mask= 不能忽略
        img1_bg = cv2.bitwise_and(roi,roi,mask = mask)
        # 取 roi 中与 mask_inv 中不为零的值对应的像素的值，其他值为 0。
        # Take only region of logo from logo image.
        img2_fg = cv2.bitwise_and(img2,img2,mask = mask_inv)
        # Put logo in ROI and modify the main image
        dst = cv2.add(img1_bg,img2_fg)
        img3 = img1
        img3[0:rows, 0:cols ] = dst
        
        cv2.imshow('res',img3)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
    def test_efficiency(self):
        #测试程序运行效率
        img1 = cv2.imread('opencvlogo.jpg')
        e1 = cv2.getTickCount()
        for i in range(5,49,2):
            print(i)
            img1 = cv2.medianBlur(img1,i)
        e2 = cv2.getTickCount()
        t = (e2 - e1)/cv2.getTickFrequency()
        print(t)

        cv2.imshow('res',img1)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def object_trace(self):
        # 物体追踪
        # hsv空间比较通俗的解释 https://zhuanlan.zhihu.com/p/67930839
        # 百度百科 https://baike.baidu.com/item/HSV/547122?fromtitle=HSV%E9%A2%9C%E8%89%B2%E7%A9%BA%E9%97%B4&fromid=12630604&fr=aladdin
        # hsv在线转换https://c.runoob.com/front-end/868/        
        '''
        for i in dir(cv2):
            if i.startswith('COLOR_'):
                print(i)
        '''        
        #blue = np.uint8([[[255,0,0], [255,0,0]], [[255,255,120],[255,0,0]]])
        blue = np.uint8([[[255, 0, 0], [11,12,13]]])
        print(blue.shape)
        hsv_blue = cv2.cvtColor(blue, cv2.COLOR_BGR2HSV)
        test_array = np.uint([[[1,2,3], [4,5,6]],[[1,2,3], [4,5,6]]])
        #test_array = np.uint([[1, 2, 3]])
        print(hsv_blue, blue.shape, test_array.shape)
        #print(hsv_blue)
        '''
        #视频跟踪演示
        cap=cv2.VideoCapture(0)
        while(1):
            # 获取每一帧
            ret,frame=cap.read()
            # 转换到 HSV
            hsv=cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
            # 设定蓝色的阈值,hsv空间
            lower_blue=np.array([100,50,50])
            upper_blue=np.array([150,255,255])
            # 根据阈值构建掩模
            mask=cv2.inRange(hsv,lower_blue,upper_blue)
            # 对原图像和掩模进行位运算
            res=cv2.bitwise_and(frame,frame,mask=mask)
            # 显示图像
            cv2.imshow('frame',frame)
            #cv2.imshow('hsv',hsv)
            cv2.imshow('res',res)
            k=cv2.waitKey(5)
            if k==27:
                break
            # 关闭窗口
        cv2.destroyAllWindows()
        '''
    def show_img(self, img, title):
        '''
        @brief:把图像显示三句函数整合为一个函数
        '''
        cv2.imshow(title, img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
    def img_reshape(self):
        '''
        @brief:图像的扩展缩放、平移、旋转、仿射变换
        '''
        
        img=cv2.imread('特斯拉.jpg')
        '''
        #res=cv2.resize(img,None,fx=2,fy=2,interpolation=cv2.INTER_CUBIC)
        res=cv2.resize(img,dsize=(2160, 1440),fx=1.5,fy=2,interpolation=cv2.INTER_CUBIC)
        print(img.shape, res.shape)
        cv2.imwrite('double特斯拉.jpg', res)
        self.show_img(img, 'img')
        self.show_img(res, 'res')
        

        # 演示图像平移
        rcshape = img.shape
        rows = rcshape[0]
        cols = rcshape[1]
        print('rcshape', rows, cols)
        M = np.float32([[1,0,-100],[0,1,50]])
        dst = cv2.warpAffine(img,M,(cols, rows))#此处要注意一下，dsize和rows、cols要反过来。
        self.show_img(dst, 'img_translation')
        
        rcshape = img.shape
        rows = rcshape[0]
        cols = rcshape[1]
        # 演示图像旋转
        #M1=cv2.getRotationMatrix2D((cols/2,rows/2),45,0.5)
        M1 = cv2.getRotationMatrix2D((0,0),45,1) 
        dst = cv2.warpAffine(img, M1, (cols, rows))
        print('M1:', M1)
        self.show_img(dst,'img_rotation')
        '''
        # 仿射变换
        # https://zhuanlan.zhihu.com/p/24591720
        # 在变换先后固定顶点的像素值不变，图像整体则根据变换规则进行变换
        

        rcshape = img.shape
        rows = rcshape[0]
        cols = rcshape[1]
        pts1=np.float32([[50,100],[200,50],[50,200]])
        pts2=np.float32([[100,0],[50,50],[0,100]])
        M=cv2.getAffineTransform(pts1,pts2)
        dst=cv2.warpAffine(img,M,(cols,rows))

        for i in range(3):
            print((pts1[i,0],pts1[i,1]), type(pts1[i,0]))
            cv2.drawMarker(img, (int(pts1[i,0]),int(pts1[i,1])), color = (0,255,0), markerType=cv2.MARKER_CROSS,thickness= 5)
            cv2.drawMarker(dst, (int(pts2[i,0]),int(pts2[i,1])), color = (0,255,0), markerType=cv2.MARKER_CROSS,thickness= 5)
        cv2.imshow('src', img)
        cv2.imshow('dst', dst)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        '''
        '''

        # 透视变换
        
        img = cv2.imread('road.jpg')
        rcshape = img.shape
        rows = rcshape[0]
        cols = rcshape[1]

        print('rcshape', rcshape)
        pts1=np.float32([[348,257],[406,258],[330,366],[473,369]])
        pts2=np.float32([[200,0],[400,0],[200,300],[400,300]])
        
        # 三个点无法完成透视变换
        #pts1=np.float32([[56,65],[368,52],[28,387]])
        #pts2=np.float32([[0,0],[300,0],[0,300]])
        
        M=cv2.getPerspectiveTransform(pts1,pts2)
        dst=cv2.warpPerspective(img,M,(cols,rows))
        print('M:', M)
        #dst=cv2.warpPerspective(img,M)
        
        for i in range(4):
            print((pts1[i,0],pts1[i,1]), type(pts1[i,0]))
            cv2.drawMarker(img, (int(pts1[i,0]),int(pts1[i,1])), color = (0,255,0), markerType=cv2.MARKER_CROSS,thickness= 5)
            cv2.drawMarker(dst, (int(pts2[i,0]),int(pts2[i,1])), color = (0,255,0), markerType=cv2.MARKER_CROSS,thickness= 5)
        cv2.imshow('src', img)
        cv2.imshow('dst', dst)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
    def adaptive_thresh(self):
        #自适应阈值:
        img = cv2.imread('书上文字.jpg',0) # 中值滤波
        img = cv2.medianBlur(img,5)
        ret,th1 = cv2.threshold(img,100,255,cv2.THRESH_BINARY)
        #11 为 Block size, 2 为 C 值
        th2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
        cv2.THRESH_BINARY,11,2)
        th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
        cv2.THRESH_BINARY,11,2)
        self.show_img(th2,'th1')

    def test_ploty_ostu(self):
        '''
        测试plotly功能
        ostu说明：https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html?highlight=thresh_otsu
        '''
        img = cv2.imread('road.jpg',0)
        img1 = np.ravel(img)
        print('type', type(img1), img1.shape, img1)
        
        pyplt = py.offline.plot
        data = [go.Histogram(x=img1,
                            histnorm = 'probability')] 

        ret, th2 = cv2.threshold(img,0,255,cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        #ret, th2 = cv2.threshold(img, 120, 255, cv2.THRESH_BINARY)
        print('ret', ret, cv2.THRESH_BINARY_INV, cv2.THRESH_OTSU)

        # y = x 水平直方图，histnorm='probability' y轴显示概率，没有则显示数目
        #pyplt(data, filename='1.html')

        pyplt(data)
        self.show_img(img, 'road')
        self.show_img(th2, 'th2')

    def test_ostu_princple(self):
        '''
        测试ostu阈值法原理
        '''
        img = cv2.imread('途.jpg', 0)
        img1 = np.ravel(img)
        pyplt = py.offline.plot
        data = [go.Histogram(x=img1,
                            histnorm = 'probability')]
        pyplt(data)
        
        blur = cv2.GaussianBlur(img,(5,5),0)
        # find normalized_histogram, and its cumulative distribution function
        hist = cv2.calcHist([blur],[0],None,[256],[0,256])
        #print(hist, type(hist), hist.shape)

        hist_norm = hist.ravel()/hist.max()
        Q = hist_norm.cumsum()
        #print('hist_norm', hist_norm, 'hist.max()', hist.max(), Q)
        #help(np.cumsum())
        
        bins = np.arange(256)

        fn_min = np.inf
        thresh = -1

        #print('bins', bins, fn_min)

        for i in range(1,256):
            p1,p2 = np.hsplit(hist_norm,[i]) # probabilities
            print('i', i, 'p1,p2', p1, '---', p2)
            q1,q2 = Q[i],Q[255]-Q[i] # cum sum of classes
            b1,b2 = np.hsplit(bins,[i]) # weights

            # finding means and variances
            m1,m2 = np.sum(p1*b1)/q1, np.sum(p2*b2)/q2
            #v1,v2 = np.sum(((b1-m1)**2)*p1)/q1, np.sum(((b2-m2)**2)*p2)/q2
            v1,v2 = np.sum(((b1-m1)**2)*p1), np.sum(((b2-m2)**2)*p2)
            fn = v1 + v2

            # calculates the minimization function
            #fn = v1*q1 + v2*q2

            if fn < fn_min:
                fn_min = fn
                thresh = i
                
        # find otsu's threshold value with OpenCV function
        ret, otsu = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        print (thresh,ret)

    def test_convolution(self):
        '''
        @brief:测试图像卷积运算
        '''
        #src = np.uint8([[0,255,0,255], [0,255,0,255],[0,255,0,255], [0,255,0,255]])
        #kernel = np.float32([[0.4,0.7,0.4],[0.7,1,0.7],[0.4,0.7,0.4]])*1/9
        ##src = np.ones((5,5), np.float32)
        #src = np.uint8([[1,1,1,1,1], [1,2,3,2,1],[1,1,1,1,1], [1,2,3,2,1], [1,1,1,1,1]])
        kernel = np.ones((5,5),np.float32)/25
        #dst = cv2.filter2D(src, -1, kernel,borderType=cv2.BORDER_ISOLATED)
        #dst = cv2.filter2D(src, -1, kernel,borderType=cv2.BORDER_CONSTANT)
        
        src = cv2.imread('road.jpg', 0)
        #dst = cv2.filter2D(src, -1, kernel)
        #dst = cv2.GaussianBlur(src,(5,5),0)
        #dst = cv2.medianBlur(src,5)
        dst = cv2.bilateralFilter(src,9, 75, 75)
        #print('src:', src)
        #print('dst:', dst)
        #print('kernel:', kernel)

        self.show_img(src,'src')
        self.show_img(dst,'dst')
    
    def test_canny(self):
        '''
        @brief:测试canny边缘检测
        '''
        img = cv2.imread('特斯拉.jpg',0)
        edges = cv2.Canny(img,100,200)
        print('edges', edges)
        self.show_img(img, 'src')
        self.show_img(edges, 'edges')
        
    def test_morphological(self):
        '''
        @brief:测试形态学变换功能
        '''
        img = cv2.imread('j.png',0)
    
        #img = np.uint8([[1,2,3,4,5], [5,4,3,2,1],[2,4,6,8,10], [10,8,6,4,2], [5,10,15,10,5]])
        
        kernel = np.ones((3,3),np.uint8)
        #erosion = cv2.erode(img,kernel,iterations = 1)
        dilation = cv2.dilate(img,kernel,iterations = 1)
        self.show_img(img, 'src')
        #self.show_img(erosion, 'erosion')
        self.show_img(dilation, 'dilation')

        #print('img:', img)
        #print('erosion:', erosion)

    def test_pyramid(self):
        '''
        @brief:测试图像金字塔
        '''
        '''
        img = cv2.imread('road.jpg')
        for i in range(10):
            print('size:', img.shape)
            self.show_img(img, 'pydown')
            #img = cv2.pyrDown(img)
            img = cv2.pyrUp(img)
            #print('part:', img[0:5,0:5,0])
        
        '''

        #测试上采样效果
        img_test = np.uint8([[1, 2, 3, 4, 5],
                             [6, 7, 8, 9, 10],
                             [11, 12, 13, 14, 15],
                             [16, 17, 18, 19, 20],
                             [21, 22, 23, 24, 25]])
        print('src:', img_test)
        img_test = cv2.pyrUp(img_test)
        print('dst:', img_test)
        

    def test_imgblending(self):
        '''
        @brief：测试图像融合
        '''
        A = cv2.imread('apple.jpg')
        B = cv2.imread('orange.jpg')

        # generate Gaussian pyramid for A
        G = A.copy()
        gpA = [G]
        for i in range(6):
            G = cv2.pyrDown(G)
            gpA.append(G)

        # generate Gaussian pyramid for B
        G = B.copy()
        gpB = [G]
        for i in range(6):
            G = cv2.pyrDown(G)
            gpB.append(G)

        # generate Laplacian Pyramid for A
        lpA = [gpA[5]]
        for i in range(5,0,-1):
            print('i:', i)
            GE = cv2.pyrUp(gpA[i])
            L = cv2.subtract(gpA[i-1],GE)
            lpA.append(L)

        # generate Laplacian Pyramid for B
        lpB = [gpB[5]]
        for i in range(5,0,-1):
            GE = cv2.pyrUp(gpB[i])
            L = cv2.subtract(gpB[i-1],GE)
            lpB.append(L)

        # Now add left and right halves of images in each level
        LS = []
        # https://www.runoob.com/python/python-func-zip.html
        for la,lb in zip(lpA,lpB):
            rows,cols,dpt = la.shape
            #print('cols/2', cols/2, type(cols/2))
            '''
            cv2.imshow('imga',la)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            
            cv2.imshow('imgb',lb)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            '''
            
            # https://blog.csdn.net/m0_37393514/article/details/79538748
            ls = np.hstack((la[:,0:int(cols/2)], lb[:,int(cols/2):]))
            '''
            cv2.imshow('hstackab',ls)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            '''
            LS.append(ls)

        # now reconstruct
        ls_ = LS[0]
        for i in range(1,6):
            ls_ = cv2.pyrUp(ls_)
            ls_ = cv2.add(ls_, LS[i])

        # image with direct connecting each half
        real = np.hstack((A[:,:int(cols/2)],B[:,int(cols/2):]))


        #self.show_img('Pyramid_blending2', ls_)

        cv2.imwrite('Pyramid_blending2.jpg',ls_)
        cv2.imwrite('Direct_blending.jpg',real)

    def test_contours(self):
        '''
        @brief:测试轮廓功能
        '''
        img = cv2.imread('black white.jpg')
        imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(imgray, 127, 255, cv2.THRESH_BINARY)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
        
        cnt = contours[2]
        print('contours:', contours, cnt.shape, len(contours))
        M = cv2.moments(cnt)
        print('M:',M)
        #epsilon = 0.0001*cv2.arcLength(cnt,True)
        #print('epsilon', epsilon)
        #approx = cv2.approxPolyDP(cnt,epsilon,True)      

        hull = cv2.convexHull(cnt)
        cv2.drawContours(img, contours[2], -1, (0,255,0), 10)

        #print('contours:', contours[0], cnt.shape, len(contours))
        #print('hull:', hull, type(hull))
        self.show_img(img, 'contours')

    def test_houghlines(self):
        '''
        @测试霍夫变换
        '''
        
        img = cv2.imread('squares.jpg')
        l0 = img.shape
        print('l0:', l0[0])
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(gray,50,200,apertureSize = 3)
        lines = cv2.HoughLines(edges,1,np.pi/180,300)
        print('lines', lines, type(lines), lines.shape)
        for line in lines:
            for rho,theta in line:
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a*rho
                y0 = b*rho
                x1 = int(x0 + 1000*(-b))
                y1 = int(y0 + 1000*(a))
                x2 = int(x0 - 1000*(-b))
                y2 = int(y0 - 1000*(a))
                cv2.line(img,(x1,y1),(x2,y2),(0,0,255),3)
        self.show_img(edges, 'canny')
        self.show_img(img,'results')
        cv2.imwrite('houghlines3.jpg',img)
    def test_houghlinesp(self):
        '''
        @brief：验证houghlinesp函数功能
        '''
        img = cv2.imread('squares.jpg')
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(gray,50,200,apertureSize = 3)
        minLineLength = 100
        maxLineGap = 10
        lines = cv2.HoughLinesP(edges,1,np.pi/180,100,minLineLength,maxLineGap)
        for line in lines:
            print('line:', line)
            for x1,y1,x2,y2 in line :
                cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
        self.show_img(img,'results')
        #cv2.imwrite('houghlines5.jpg',img)
    
    def test_houghcircles(self):
        '''
        @brief：霍夫圆检测
        '''
        #img = cv2.imread('singlecircle.jpg',0)
        #img = cv2.imread('mulcircles.png', 0)
        img = cv2.imread('RR.png', 0)
        img = cv2.imread('LL.png', 0)
        print('shape:', img.shape)
        #img = cv2.medianBlur(img,5)
        cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
        #circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,minDist=50,param1=10,param2=50,minRadius=0,maxRadius=0)
        #circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1, minDist=30, param1=10,param2=20,minRadius=10,maxRadius=15)#RR

        circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1, minDist=25, param1=10,param2=20,minRadius=9,maxRadius=15)#LL


        #circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1, 100, 10,20,1,100)
        #print('circles1:', circles)
        circles = np.uint16(np.around(circles))
        #print('circles2:', circles)
        
        for i in circles[0,:]:
            # draw the outer circle
            cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
            # draw the center of the circle
            cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)
        self.show_img(cimg, 'cimg')

    def testFeatureMatching(self):
        '''
        @brief:测试特征匹配效果
        '''
        bf = cv2.BFMatcher(cv2.NORM_HAMMING, crosscheck=True)