#!/usr/bin/env python
import sys
import threading
# win:
# sys.path.append('C:\\Users\\Administrator\\Desktop\\geo-meas-master\\src')
# mac:
sys.path.append('/Users/huangzhe/Desktop/geo-meas/src')

import PySimpleGUI as sg
from numpy import size
import cv2 as cv
import layouts as ly
import config as cg
import imgproc as imgp

if sys.platform.startswith('win'):
    #import winsound
    ''' 
    sg.popup_error('Sorry, you gotta be on Windows')
    sys.exit()
    '''

main_layout = ly.Layout().layoutMain(sg)

window = sg.Window("管理系统", main_layout, auto_size_buttons=False, 
                   default_button_element_size=(20,1), use_default_focus=True, finalize=True)

recording = have_data = False

ViedoTest_flag = 0
while True:
    event, values = window.read(timeout=100)
    if event == sg.WINDOW_CLOSED:
        break
    if event == '-BinocularVision-':
        layout_temp = ly.Layout().layoutBinocularVisionMeasurement(sg)
        window_BinocularVisionMeasurement = sg.Window("双目视觉测量系统", layout_temp, auto_size_buttons=True, 
                   default_button_element_size=(15,1), use_default_focus=True, finalize=True)
        threading.Thread(target=cg.ThreadFunctions().binocularVisionProc, args=('Thread 1', 1000, window_BinocularVisionMeasurement,),  daemon=True).start()
        sg.cprint_set_output_destination(window_BinocularVisionMeasurement, '-MultiLines_BinocularVision-')
        while True:
            event_BinocularVisionMeasurement, values_BinocularVisionMeasurement = window_BinocularVisionMeasurement.read()
            if event_BinocularVisionMeasurement in (sg.WIN_CLOSED, 'Exit'):
                break
            sg.cprint(event_BinocularVisionMeasurement, values_BinocularVisionMeasurement[event_BinocularVisionMeasurement], c=cg.ThreadFunctions().colors[event_BinocularVisionMeasurement])
            
    if event == '-DigitalImage-':
        #数字图像处理
        layout_temp = ly.Layout().layoutDigitalImageProc(sg)
        window_DigitalImage = sg.Window("数字图像处理", layout_temp, auto_size_buttons=True, 
                   default_button_element_size=(15,1), use_default_focus=True, finalize=True)
        while True:
            event_DigitalImage, values_DigitalImage = window_DigitalImage.read(timeout=100)
            if event_DigitalImage == sg.WINDOW_CLOSED:
                break
            if event_DigitalImage == '-CenterCapture-':
                print('进入质心提取...')
                #----------添加事件处理函数-------------
                imgp.Basicproc().calSimThroreticalCenter(w=1280, h=1024, radius=20,randomrange_up=800)
                #----------END-------------
                continue
                
    if event == '-ViedoTest-':
        #电脑自带摄像头测试，基于opencv
        if ViedoTest_flag == 0:
            cap = cv.VideoCapture(0)
            layout_vt = ly.Layout().layoutVideoTest(sg)
            window_vt=sg.Window("2", layout_vt, auto_size_buttons=True, 
                            default_button_element_size=(15,1), use_default_focus=True, finalize=True)
            ViedoTest_flag = 1
            
    if ViedoTest_flag == 1:#子窗口打开后
        event_bv, values_bv = window_vt.read(timeout=100)
        if event_bv == 'Exit' or event_bv == sg.WIN_CLOSED:
            ViedoTest_flag = 0
            cap.release()
            continue
        ret, frame = cap.read()    
        imgbytes = cv.imencode('.png', frame)[1].tobytes()
        window_vt['-IMAGE-'].update(data=imgbytes)
    #winsound.PlaySound("ButtonClick.wav", 1) if event != sg.TIMEOUT_KEY else None
window.close()