'''
import sys
import os
curpath = os.path.abspath(os.path.dirname(__file__))
rootpath = os.path.split(curpath)[0]
sys.path.append(rootpath)
'''
#from msilib.schema import Class
import PySimpleGUI as sg

class Layout(object):
    def __init__(self, *args):
        super(Layout, self).__init__(*args)
        
    def layoutBinocularVisionMeasurement(self, sg):
        '''
        双目视觉界面
        '''   
        layout = [
            [sg.Frame('相机1:',[[ sg.Graph((640,512),(0,512), (640,0), key='-GRAPH1-', enable_events=True, drag_submits=True, visible=True, background_color='grey')]],),
             sg.Frame('相机2:',[[ sg.Graph((640,512),(0,512), (640,0), key='-GRAPH2-', enable_events=True, drag_submits=True, visible=True, background_color='grey')]],)],
            
            [sg.Frame('调试信息:',[[sg.Multiline(size=(183, 15), key='-MultiLines_BinocularVision-', autoscroll=True)]],)],
            
            [sg.Frame('控制:',[[sg.Button('打开相机', button_color=('white','green'), key='-DigitalImage-')],
                            [sg.Button('关闭相机', button_color=('white','green'), key='-ViedoTest-')]],)],
            ]
        return layout    
    def layoutMain(self, sg):
        '''
        主框架
        '''   
        layout = [
            [sg.Frame('教学:',
                      [[sg.Button('Digital Image Processing', button_color=('white','green'), key='-DigitalImage-')]],)],
            
            [sg.Frame('科研:',
                      [[sg.Button('Opencv Video', button_color=('white','green'), key='-ViedoTest-')],
                       [sg.Button('Monocular Vision', button_color=('white', 'green'), key='-MonocularVision-')],
                       [sg.Button('Binocular Vision', button_color=('white', 'green'), key='-BinocularVision-')],
                       [sg.Button('Line Structured Light', button_color=('white', 'green'), key='-LineStructuredLight-')]
                       ],
                       
                      
                    )],
            ]
        return layout
     
    def layoutDigitalImageProc(self, sg):
        '''
        数字图像处理子布局
        '''
        layout = [
                
                [sg.Frame('相机1:',[[ sg.Graph((640,512),(0,512), (640,0), key='-GRAPH1-', enable_events=True, drag_submits=True, visible=True, background_color='grey')]],)],
                [sg.Button('单点质心提取', button_color=('white', 'green'), key='-CenterCapture-')]]
        return layout
    def layoutVideoTest(self, sg):
        '''
        双目视觉
        '''
        layout = [
                    [sg.Text('OpenCV Demo', size=(60, 1), justification='center')],
                    [sg.Image(filename='', key='-IMAGE-')],
                    [sg.Radio('None', 'Radio', True, size=(10, 1))],
                    [sg.Radio('threshold', 'Radio', size=(10, 1), key='-THRESH-'),
                    sg.Slider((0, 255), 128, 1, orientation='h', size=(40, 15), key='-THRESH SLIDER-')],
                    [sg.Radio('canny', 'Radio', size=(10, 1), key='-CANNY-'),
                    sg.Slider((0, 255), 128, 1, orientation='h', size=(20, 15), key='-CANNY SLIDER A-'),
                    sg.Slider((0, 255), 128, 1, orientation='h', size=(20, 15), key='-CANNY SLIDER B-')],
                    [sg.Radio('blur', 'Radio', size=(10, 1), key='-BLUR-'),
                    sg.Slider((1, 11), 1, 1, orientation='h', size=(40, 15), key='-BLUR SLIDER-')],
                    [sg.Radio('hue', 'Radio', size=(10, 1), key='-HUE-'),
                    sg.Slider((0, 225), 0, 1, orientation='h', size=(40, 15), key='-HUE SLIDER-')],
                    [sg.Radio('enhance', 'Radio', size=(10, 1), key='-ENHANCE-'),
                    sg.Slider((1, 255), 128, 1, orientation='h', size=(40, 15), key='-ENHANCE SLIDER-')],
                    [sg.Button('Exit', size=(10, 1))]
                ]
        return layout
    
if __name__ == "__main__":
    ly = Layout()
    window = sg.Window("测试界面", ly.layoutBinocularVisionMeasurement(sg), auto_size_buttons=False, 
                   default_button_element_size=(15,1), use_default_focus=True, finalize=True)
    while True:
        event, values = window.read(timeout=100)
        if event == sg.WINDOW_CLOSED:
            break