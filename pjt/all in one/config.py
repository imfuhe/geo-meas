#!/usr/bin/python3
import threading
import time
import itertools
import PySimpleGUI as sg
import imgproc as imgp
import cv2


class BaslerCamera(object):
    '''
    Basler相机相关操作
    20220415 by Hz
    '''
    def __init__(self) -> None:
        pass

class ZED2Camera(object):
    '''
    Zed相机相关操作
    20220415 by Hz
    '''
    def __init__(self) -> None:
        pass
    
class ThreadFunctions(object):
    '''
    线程函数
    '''
    colors = {'Thread 1':('white', 'red'), 'Thread 2':('white', 'purple'), 'Thread 3':('white', 'blue')}
    def __init__(self) -> None:
        pass
    
    def binocularVisionProc(self, thread_name, run_freq,  window):
        """
        A worker thread that communicates with the GUI
        These threads can call functions that block without affecting the GUI (a good thing)
        Note that this function is the code started as each thread. All threads are identical in this way
        :param thread_name: Text name used  for displaying info
        :param run_freq: How often the thread should run in milliseconds
        :param window: window this thread will be conversing with
        :type window: sg.Window
        :return:
        """
        id1 = None
        id2 = None
        graph_elem1 = window['-GRAPH1-']      # type: sg.Graph
        graph_elem2 = window['-GRAPH2-']
        sg.cprint_set_output_destination(window, '-MultiLines_BinocularVision-')
        
        for i in itertools.count():                             # loop forever, keeping count in i as it loops
            time.sleep(run_freq/1000)                           # sleep for a while
            
            frame1,cx1,cy1 = imgp.Basicproc().calSimThroreticalCenterOneTime(w=1280, h=1024, radius=20,randomrange_up=800)
            frame2,cx2,cy2 = imgp.Basicproc().calSimThroreticalCenterOneTime(w=1280, h=1024, radius=20,randomrange_up=800)
            frame1 = cv2.resize(frame1, dsize=(640,512))
            frame2 = cv2.resize(frame2, dsize=(640,512))
            
            imgbytes1=cv2.imencode('.ppm', frame1)[1].tobytes()
            imgbytes2=cv2.imencode('.ppm', frame2)[1].tobytes()
            if id1:
                graph_elem1.delete_figure(id1)             # delete previous image
            id1 = graph_elem1.draw_image(data=imgbytes1, location=(0,0))    # draw new image
            graph_elem1.send_figure_to_back(id1)            # move image to the "bottom" of all other drawings
            
            if id2:
                graph_elem2.delete_figure(id2)
            id2 = graph_elem2.draw_image(data=imgbytes2, location=(0,0))    # draw new image
            graph_elem2.send_figure_to_back(id1)            # move image to the "bottom" of all other drawings
            
            # put a message into queue for GUI
            #window.write_event_value(thread_name, f'count = {i}')
            sg.cprint('相机1质心坐标：'+str(cx1)+','+str(cy1)+'----'+'相机2质心坐标：'+str(cx2)+','+str(cy2)+'----'+'目标点坐标：XXX.XXXmm', text_color='black',background_color='yellow')
        
if __name__ == "__main__":
    print('i am in config.py')