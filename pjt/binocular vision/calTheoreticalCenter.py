'''
轮廓法模拟质心提取过程byHZ@2022-4-5，空格键继续，ESC退出
'''
import sys
import numpy as np
import cv2 as cv
#根据电脑实际路径添加Geomeas库
sys.path.append('C:\\Users\\Administrator\\Desktop\\geo-meas-master\\src')
import imgproc as imgp

W = 1280#宽度（列数）
H = 1024#高度（行数）
CENTER = (640, 512)#理论中心
R = 20
RANDOMRANGE_UP = 800#不能超过1024
i = 0
#for i in range(1000):
while True:
    centerlist = []
    img = imgp.Basicproc().genEmptyImage(w=W, h=H, opt='color')
    title = 'empty img' + str(img.shape)
    
    rnd = (np.random.randint(1,RANDOMRANGE_UP)-int(RANDOMRANGE_UP/2), np.random.randint(1,RANDOMRANGE_UP)-int(RANDOMRANGE_UP/2))#随机生成不同位置
    cxcy = (CENTER[0]+rnd[0],CENTER[1]+rnd[1])
    cv.circle(img, center=cxcy, radius=R, color=(255,255,255), thickness=-1)
    imgray = cv.cvtColor(img, code=cv.COLOR_RGB2GRAY)

    #轮廓法提取光斑质心
    ret, thresh = cv.threshold(imgray, 127, 255, cv.THRESH_BINARY)
    contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    #找全部质心
    for index in range(len(contours)):
        M = cv.moments(contours[index])
        cx = round(M['m10'] / M['m00'], 3)  # 算质心
        cy = round(M['m01'] / M['m00'], 3)    
        centerlist.append([cx, cy])
    #封装函数结束
    
    cv.drawContours(img,contours=contours,contourIdx=-1,color=imgp.COLORS['red'],thickness=2)    
    cv.drawMarker(img,position=cxcy,color=imgp.COLORS['red'],markerType=cv.MARKER_CROSS,thickness=2)
    print('理论中心：',cxcy,' 计算质心：', i,centerlist)
    i = i+1
    #cv.imwrite('empty.png', img)
    cv.imshow(title, img)
    k = cv.waitKey(0)
    if k == 27:
        cv.destroyAllWindows()
        break